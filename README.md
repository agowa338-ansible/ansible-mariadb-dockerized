MariaDB Docker
=========

Spawns a mariadb docker container

Role Variables
--------------

| Name                      | Default Value      | Usage                                                     |
|---------------------------|--------------------|-----------------------------------------------------------|
| mariadb_project_directory | '/srv/mariadb'     | Directory on the host where the MariaDB databaseis stored |
| mariadb_container_name    | 'MariaDB'          | Name of the MariaDB instance                              |
| mariadb_base_image        | 'mariadb'          | Docker base image                                         |

License
-------

MIT
